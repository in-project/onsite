package com.example.asus.onsiteskripsi.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.asus.onsiteskripsi.Absensi;
import com.example.asus.onsiteskripsi.LaporanAbsenActivity;
import com.example.asus.onsiteskripsi.R;
import com.example.asus.onsiteskripsi.SplashActivity;


public class HomeFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    CardView c1,c2,c3;

    public HomeFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_home, container, false);

        c1=view.findViewById(R.id.cv_ab_in);
        c2=view.findViewById(R.id.cv_ab_out);
        c3=view.findViewById(R.id.cv_laporan);


        c1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(), Absensi.class);
                intent.putExtra("jenis","Absen In");
                startActivity(intent);
            }
        });
        c2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(), Absensi.class);
                intent.putExtra("jenis","Absen Out");
                startActivity(intent);
            }
        });
        c3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), LaporanAbsenActivity.class));
            }
        });

        // Inflate the layout for this fragment
        return view;
    }
}
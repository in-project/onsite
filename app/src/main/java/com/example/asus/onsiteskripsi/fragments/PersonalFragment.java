package com.example.asus.onsiteskripsi.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.asus.onsiteskripsi.LoginActivity;
import com.example.asus.onsiteskripsi.R;
import com.example.asus.onsiteskripsi.utils.PrefManager;


public class PersonalFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    PrefManager prefManager;
    TextView tvNama,tvId,tvDivisi,tvEmail,tvButton;

    public PersonalFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefManager=new PrefManager(getActivity());
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_personal, container, false);
        tvNama=view.findViewById(R.id.tv_nama);
        tvId=view.findViewById(R.id.tv_id);
        tvDivisi=view.findViewById(R.id.tv_divisi);
        tvEmail=view.findViewById(R.id.tv_email);
        tvButton=view.findViewById(R.id.btn_logout);

        tvNama.setText(prefManager.getUsername());
        tvId.setText(prefManager.getId());
        tvDivisi.setText(prefManager.getDivisi());
        tvEmail.setText(prefManager.getEmail());

        tvButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                prefManager.setCekLogin(PrefManager.CEK_LOGIN,false);
                startActivity(new Intent(getActivity(), LoginActivity.class));
                getActivity().finish();
            }
        });


        return view;
    }


}
package com.example.asus.onsiteskripsi.utils;

import com.example.asus.onsiteskripsi.responses.AbsenInResponse;
import com.example.asus.onsiteskripsi.responses.LoginResponse;
import com.example.asus.onsiteskripsi.responses.RegisterResponse;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by Dossy on 6/24/2018.
 */

public interface ApiEndPoint {

    @FormUrlEncoded
    @POST("login.php")
    Call<LoginResponse> loginRequest(@Field("email") String email,
                                     @Field("password") String password);


    @FormUrlEncoded
    @POST("register.php")
    Call<RegisterResponse> register(@Field("id_karyawan") String Id,
                                    @Field("divisi") String Divisi,
                                    @Field("nama") String Nama,
                                    @Field("email") String Email,
                                    @Field("password") String Password);

    @Multipart
    @POST("absen.php")
    Call<AbsenInResponse> absen(@Part("key") String key,
                                @Part("kegiatan") String kegiatan,
                                @Part MultipartBody.Part file,
                                @Part("lokasi") String lokasi,
                                @Part("jam") String jam,
                                @Part("tanggal") String tanggal,
                                @Part("id_karyawan") String idKaryawan,
                                @Part("divisi") String divisi);


}
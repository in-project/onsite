package com.example.asus.onsiteskripsi;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.asus.onsiteskripsi.responses.RegisterResponse;
import com.example.asus.onsiteskripsi.utils.ApiClient;
import com.example.asus.onsiteskripsi.utils.ApiEndPoint;
import com.example.asus.onsiteskripsi.utils.PopupUtil;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Register extends AppCompatActivity {

    // UI references.
    private EditText et_id, et_nama, et_email, et_password, et_confirm,et_divisi;
    Button btn_register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        // Set up the login form.
        et_id = findViewById(R.id.et_id);
        et_divisi = (EditText) findViewById(R.id.et_divisi);
        et_nama = (EditText) findViewById(R.id.et_nama);
        et_email = (EditText) findViewById(R.id.et_email);
        et_password = (EditText) findViewById(R.id.et_password);
        et_confirm = (EditText) findViewById(R.id.et_confirm);
        btn_register = (Button) findViewById(R.id.btnsignup);


        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String id=et_id.getText().toString();
                String nama=et_nama.getText().toString();
                String divisi=et_divisi.getText().toString();
                String email=et_email.getText().toString();
                String password=et_password.getText().toString();
                String confirm=et_confirm.getText().toString();

                if (!password.equals(confirm)){
                    et_confirm.setError(" Password Tidak Sama");
                }else{
                    register(id,nama,divisi,email,password);
                }


            }
        });

    }

    
    public void register(String id, String nama, String divisi,String email, String password){

        PopupUtil.showLoading(this,"Register In..","Please Wait ..");

        ApiEndPoint apiEndPoint= ApiClient.getClient().create(ApiEndPoint.class);
        Call<RegisterResponse> call=apiEndPoint.register(id,nama,divisi,email,password);

        call.enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                PopupUtil.dismissDialog();

                Toast.makeText(getApplicationContext(), "Berhasil Daftar", Toast.LENGTH_LONG).show();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        startActivity(new Intent(getApplicationContext(), LoginActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                        finish();
                    }
                });

            }

            @Override
            public void onFailure(Call<RegisterResponse> call, Throwable t) {
                PopupUtil.dismissDialog();
            }
        });
    }
}

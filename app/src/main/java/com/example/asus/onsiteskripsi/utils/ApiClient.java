package com.example.asus.onsiteskripsi.utils;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Dossy on 6/24/2018.
 */

public class ApiClient {

    //public static final String BASE_URL="http://192.168.1.26/absen_online/";
    public static final String BASE_URL="http://192.168.43.170/absen_online/api/";

    private static Retrofit retrofit=null;

    public static Retrofit getClient(){

        if (retrofit==null){
            retrofit=new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        return retrofit;
    }

}
package com.example.asus.onsiteskripsi.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Dossy on 6/24/2018.
 */

public class PrefManager {

    //Key untuk pemanggilan fungsi
    public static final String NAMA_APLIKASI="absen";
    public static final String CEK_LOGIN="cekLogin";
    public static final String USERNAME="username";
    public static final String ID="id";
    public static final String DIVISI="divisi";
    public static final String EMAIL="email";
    public static final String PASSWORD="password";


    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    public PrefManager(Context context) {
        sharedPreferences=context.
                getSharedPreferences(NAMA_APLIKASI,Context.MODE_PRIVATE);
        editor=sharedPreferences.edit();
    }

    public void setCekLogin(String key,boolean value){
        editor.putBoolean(key,value);
        editor.commit();
    }

    public void setUsername(String key,String value){
        editor.putString(key,value);
        editor.commit();
    }
    public void setId(String key,String value){
        editor.putString(key,value);
        editor.commit();
    }
    public void setDivisi(String key,String value){
        editor.putString(key,value);
        editor.commit();
    }
    public void setEmail(String key,String value){
        editor.putString(key,value);
        editor.commit();
    }

    public void setPassword(String key,String value){
        editor.putString(key,value);
        editor.commit();
    }

    public Boolean getCekLogin() {
        return sharedPreferences.getBoolean(CEK_LOGIN,false);
    }

    public String getUsername() {
        return sharedPreferences.getString(USERNAME,"admin");
    }
    public String getId() {
        return sharedPreferences.getString(ID,"id");
    }
    public String getDivisi() {
        return sharedPreferences.getString(DIVISI,"divisi");
    }

    public String getEmail() {
        return sharedPreferences.getString(EMAIL,"email");
    }

    public String getPassword() {
        return sharedPreferences.getString(PASSWORD,"1234");
    }

}
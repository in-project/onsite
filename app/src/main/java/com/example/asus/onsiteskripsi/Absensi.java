package com.example.asus.onsiteskripsi;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.asus.onsiteskripsi.responses.AbsenInResponse;
import com.example.asus.onsiteskripsi.utils.ApiClient;
import com.example.asus.onsiteskripsi.utils.ApiEndPoint;
import com.example.asus.onsiteskripsi.utils.PopupUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.joda.time.LocalDate;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Absensi extends FragmentActivity implements OnMapReadyCallback, LocationListener,
        ActivityCompat.OnRequestPermissionsResultCallback {
    private GoogleMap mMap;


    TextView tv_lokasi, tv_jam, tv_tanggal;
    LocationManager locationManager;
    double lat, lng;
    String hariIndo, waktuSekarang, jenisAbsen;
    Button btn_absen_in;
    EditText et_kegiatan;
    MultipartBody.Part body;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_absensi_masuk);

        jenisAbsen=getIntent().getStringExtra("jenis");
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        tv_lokasi = findViewById(R.id.tv_lokasi);
        tv_jam = findViewById(R.id.tv_jam);
        tv_tanggal = findViewById(R.id.tv_tanggal);
        btn_absen_in = findViewById(R.id.btn_absen_in);
        et_kegiatan = findViewById(R.id.et_kegiatan);



        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //Cek izin
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                //locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 10, 10, (LocationListener) this);

            } else {
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 10, 10, (LocationListener) this);
            }

//            PopupUtil.showLoading(this,"","Mencari lokasi anda...");
        }


        openCamera();

        //Ganti Text Font Pada Button Absen
        if (jenisAbsen.equals("Absen In")){
            btn_absen_in.setText("Absen In");
        }else{
            btn_absen_in.setText("Absen Out");
        }

        btn_absen_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String kegiatan = et_kegiatan.getText().toString();
                String lokasi = tv_lokasi.getText().toString();
                String jam = waktuSekarang;
                String tanggal = tv_tanggal.getText().toString();
                String divisi = "";
                String idKaryawan = "";

                absen_in(kegiatan, lokasi, jam, tanggal, idKaryawan, divisi);

            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
        }
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 10, 10, (LocationListener) this);
        PopupUtil.showLoading(this, "", "Mencari lokasi anda...");
    }

    ImageView img;
    static int TAKE_PIC = 1;

    private void openCamera() {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        startActivityForResult(intent, TAKE_PIC);
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        img = (ImageView) findViewById(R.id.imgv_takephoto);
        if (requestCode == TAKE_PIC && resultCode == RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            img.setImageBitmap(photo);

            File file = createTempFile(photo);

            RequestBody requestFile =
                    RequestBody.create(MediaType.parse("multipart/form-data"), file);
            body = MultipartBody.Part.createFormData("file", file.getName(), requestFile);
        }
    }



    private File createTempFile(Bitmap bitmap) {
        File file = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES)
                , System.currentTimeMillis() + ".jpg");
        ByteArrayOutputStream bos = new ByteArrayOutputStream();


        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
        byte[] bitmapdata = bos.toByteArray();
        //write the bytes in file

        try {
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        System.out.println(" Map Ready : ");
        /*
        LatLng currentLoc = new LatLng(lat, lng);
        System.out.println(" Lat : "+lat+"Lng: "+lng);
        mMap.addMarker(new MarkerOptions().position(currentLoc).title("I'am Here"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLoc,15f));
        */

    }

    @Override
    public void onLocationChanged(Location mLocation) {

        PopupUtil.dismissDialog();
        System.out.println(" Location : ");
        locationManager.removeUpdates(this);

        lat = mLocation.getLatitude();
        lng = mLocation.getLongitude();

        LatLng currentLoc = new LatLng(lat, lng);
        mMap.addMarker(new MarkerOptions().position(currentLoc).title("Lokasi Anda"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLoc, 15f));

        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(lat, lng, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName(); // Only

            //Tanggal
            Date c = Calendar.getInstance().getTime();
            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            SimpleDateFormat mdformat = new SimpleDateFormat("HH:mm:ss");
            String time = "Waktu : " + mdformat.format(c.getTime());
            waktuSekarang = mdformat.format(c.getTime());
            String formattedDate = df.format(c);
            String tanggal[] = getSplitData(df.format(c));
            String hari = new LocalDate(Integer.parseInt(tanggal[2]), Integer.parseInt(tanggal[1]), Integer.parseInt(tanggal[0])).dayOfWeek().getAsText(Locale.ENGLISH);

            hariIndo = getDayNames(hari);
            tv_jam.setText(time);
            tv_lokasi.setText(address);
            tv_tanggal.setText(hariIndo + ", " + formattedDate);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    public String[] getSplitData(String data) {
        String dateValue[] = data.split("-");
        return dateValue;
    }

    //Fungsi Buat convert ke nama bulan indo
    private String getDayNames(String day) {
        String name = "";
        if (day.equals("Sunday")) {
            name = "Minggu";
        } else if (day.equals("Monday")) {
            name = "Senin";
        } else if (day.equals("Tuesday")) {
            name = "Selasa";
        } else if (day.equals("Wednesday")) {
            name = "Rabu";
        } else if (day.equals("Thursday")) {
            name = "Kamis";
        } else if (day.equals("Friday")) {
            name = "Jum'at";
        } else if (day.equals("Saturday")) {
            name = "Sabtu";
        }

        return name;
    }

    public void absen_in(String kegiatan, String lokasi, String jam, String tanggal, String idKaryawan, String divisi) {

        PopupUtil.showLoading(this, "Absen In..", "Please Wait ..");

        ApiEndPoint apiEndPoint = ApiClient.getClient().create(ApiEndPoint.class);
        Call<AbsenInResponse> call = apiEndPoint.absen(jenisAbsen,kegiatan, body, lokasi, jam, tanggal, idKaryawan, divisi);

        call.enqueue(new Callback<AbsenInResponse>() {
            @Override
            public void onResponse(Call<AbsenInResponse> call, Response<AbsenInResponse> response) {
                PopupUtil.dismissDialog();

                Toast.makeText(getApplicationContext(), "Berhasil Absen", Toast.LENGTH_LONG).show();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        startActivity(new Intent(getApplicationContext(), MainActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                        finish();
                    }
                });

            }

            @Override
            public void onFailure(Call<AbsenInResponse> call, Throwable t) {
                PopupUtil.dismissDialog();
            }
        });
    }

}
/* <uses-permission android:name = "android.permission.Camera"/>*/
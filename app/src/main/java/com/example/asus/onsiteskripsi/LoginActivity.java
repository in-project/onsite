package com.example.asus.onsiteskripsi;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.asus.onsiteskripsi.responses.LoginResponse;
import com.example.asus.onsiteskripsi.utils.ApiClient;
import com.example.asus.onsiteskripsi.utils.ApiEndPoint;
import com.example.asus.onsiteskripsi.utils.PopupUtil;
import com.example.asus.onsiteskripsi.utils.PrefManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    // UI references.
    private EditText mPasswordView,mEmailView;
    private TextView tv_register;
    Button btn_login;

    PrefManager prefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        prefManager=new PrefManager(this);

        // Set up the login form.
        mEmailView =  findViewById(R.id.loginemail);
        mPasswordView = (EditText) findViewById(R.id.loginpassword);
        btn_login = (Button) findViewById(R.id.signin);
        tv_register=findViewById(R.id.textlink);
        //Cek Status Login
        System.out.println(" Cek Login : "+prefManager.getCekLogin());
        if (prefManager.getCekLogin()){
            //Jika sudah pernah login, langsung masuk ke main activity
            startActivity(new Intent(this,MainActivity.class));
        }

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String username=mEmailView.getText().toString();
                String password=mPasswordView.getText().toString();
                //Cek Login User dengan yg di sharedPrefManager
                    //benar
                login(username,password);

            }
        });

        tv_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),Register.class));

            }
        });
    }


    public void login(final String Username, String Password){

        PopupUtil.showLoading(this,"Logging In..","Please Wait ..");

        ApiEndPoint apiEndPoint= ApiClient.getClient().create(ApiEndPoint.class);
        Call<LoginResponse> call=apiEndPoint.loginRequest(Username,Password);

        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                PopupUtil.dismissDialog();

                final LoginResponse loginResponse=response.body();

                if (loginResponse!=null){
                    Log.d("Response Data ","Total Data"+loginResponse.getStatus());
                    if(loginResponse.getStatus()){
                        //Mendapatkan data user
                        String username=loginResponse.getUser().get(0).getNama();
                        String idKaryawan=loginResponse.getUser().get(0).getIdKaryawan();
                        String divisi=loginResponse.getUser().get(0).getDivisi();
                        String email=loginResponse.getUser().get(0).getEmail();

                        //Simpan data user ke local
                        prefManager.setUsername(PrefManager.USERNAME,username);
                        prefManager.setId(PrefManager.ID,idKaryawan);
                        prefManager.setDivisi(PrefManager.DIVISI,divisi);
                        prefManager.setEmail(PrefManager.EMAIL,email);

                        // Shared Pref ini berfungsi untuk menjadi trigger session login
                        prefManager.setCekLogin(PrefManager.CEK_LOGIN, true);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                startActivity(new Intent(getApplicationContext(), MainActivity.class));

                                finish();
                            }
                        });

                    }else{
                        Toast.makeText(getApplicationContext(), "Email Atau Password Salah", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Log.d("Login : ","Data Null");
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                PopupUtil.dismissDialog();
                Toast.makeText(getApplicationContext(), "Email Atau Password Salah", Toast.LENGTH_SHORT).show();

            }
        });
    }
}
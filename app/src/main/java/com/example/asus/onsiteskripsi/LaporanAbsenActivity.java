package com.example.asus.onsiteskripsi;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.webkit.WebSettings;
import android.webkit.WebView;

public class LaporanAbsenActivity extends AppCompatActivity {

    String url="http://192.168.43.170/Tabel/";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laporan_absen);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Terhubung ke activity_main.xml
        WebView webView=findViewById(R.id.wv_laporan);
        WebSettings webSettings=webView.getSettings();

        //Atur JavaScript
        webSettings.setJavaScriptEnabled(true);

        //Load URL
        webView.loadUrl(url);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int getId=item.getItemId();

        if (getId==android.R.id.home){
            finish();
        }


        return super.onOptionsItemSelected(item);
    }
}
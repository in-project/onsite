package com.example.asus.onsiteskripsi;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.example.asus.onsiteskripsi.fragments.HomeFragment;
import com.example.asus.onsiteskripsi.fragments.KampusFragment;
import com.example.asus.onsiteskripsi.fragments.PersonalFragment;
import com.example.asus.onsiteskripsi.fragments.PerusahaanFragment;

public class MainActivity extends AppCompatActivity {

    private BottomNavigationView bottomNavigationView;
    private FrameLayout frameLayout;

    private HomeFragment homeFragment;
    private KampusFragment kampusFragment;
    private PersonalFragment personalFragment;
    private PerusahaanFragment perusahaanFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        frameLayout = findViewById(R.id.container);
        bottomNavigationView = findViewById(R.id.navigation);

//      FRAGMENT
        homeFragment = new HomeFragment();
        kampusFragment = new KampusFragment();
        personalFragment = new PersonalFragment();
        perusahaanFragment = new PerusahaanFragment();

//        default ketika masuk kehalaman HomeUtama
        setFragment(homeFragment);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {

                    case R.id.navigation_home:
//                        bottomNavigationView.setItemBackgroundResource(R.color.colorPrimaryDark);
                        setFragment(homeFragment);
                        return true;

                    case R.id.navigation_profile1:
//                        bottomNavigationView.setItemBackgroundResource(R.color.colorPrimaryDark);
                        setFragment(perusahaanFragment);
                        return true;

                    case R.id.navigation_profile2:
//                        bottomNavigationView.setItemBackgroundResource(R.color.colorPrimaryDark);
                        setFragment(kampusFragment);
                        return true;

                    case R.id.navigation_personal:
//                        bottomNavigationView.setItemBackgroundResource(R.color.colorPrimaryDark);
                        setFragment(personalFragment);
                        return true;

                    default:
                        return false;

                }

            }


        });
    }

    private void setFragment(android.support.v4.app.Fragment fragment) {

        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment);
        fragmentTransaction.commit();
    }

}